# Aurora Assignment



## Project Description

This project creates a docker based environment, containing both a gitlab and a jenkins server.

## Prerequisites

You will need a running vm/linux server with docker and docker compose installed.


## How to run
```
git clone https://gitlab.com/yael_abramovich1/aurora-assignment.git
mkdir ./gitlab/logs ./gitlab/data ./gitlab/config
export GITLAB_HOME=./gitlab/
export JENKINS_HOME=./jenkins/jenkins_home
cd aurora-assignment
docker-compose up -d
echo 'gitlab password'
docker exec --it gitlab grep Password: /etc/gitlab/initial_root_password
```
